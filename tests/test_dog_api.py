import pytest
import allure

@allure.feature('Random_dog')
@allure.story('Получение фото случайной собаки и вложенные шаги')
def test_get_random_dog(dog_api):
    response = dog_api.get('breeds/image/random')
    with allure.step('Запрос отправлен, посмотрим код ответа'):
        assert response.status_code == 200, f'Неверный код ответа, получен {response.status_code} вместо 200'
    with allure.step('Запрос отправлен, детаилирует ответ JSON в словарь'):
        response = response.json()
        assert response['status'] == "sucsess", f"Неверный статус ответа, {response['status']} вместо sucsess"
    with allure.step(f'получили {response["status"]}'):
        with allure.step("получили еще что то"):
            with allure.step("Вложим еще один шаг"):
                pass

@allure.feature('Random_dog')
@allure.story('Получение фото случайной собаки определенной пароды')
@pytest.mark.parametrize("breed", [
    "afgan",
    "basset",
    "blood",
    "english",
    "ibizian",
    "plott",
    "walker"
])
def test_get_random_breed_image(dog_api, breed):
    response = dog_api.get(f'breeds/hound/{breed}/images/random')
    response = response.json()
    assert breed in response['message'], f'нет ссылки на изображение с указанной породой {breed}'

@allure.feature('List of dog images')
@allure.story('Список всех фото собак списком содержит только изображения')
@pytest.mark.parametrize("file", ['.md', '.MD', '.exe', '.txt'])
def test_get_breed_images(dog_api, file):
    response = dog_api.get("breed/hound/images")
    response = response.json()
    result = '\n'.join(response['message'])
    assert file not in result, f'В сообщении есть файл с расширением {file}'

@allure.feature('List of dog images')
@allure.story('Список фото определенных пород')
@pytest.mark.parametrize("breed", [
    "afgan",
    "basset",
    "blood",
    "english",
    "ibizian",
    "plott",
    "walker"
])
def test_get_random_breed_images(dog_api, breed):
    response = dog_api.get(f'breeds/hound/{breed}/images/')
    response = response.json()
    assert response['status'] == 'success', f'не удалось получить список изображений с указанной породой {breed}'

@allure.feature('List of dog images')
@allure.story('Список определенного количества случайных фото')
@pytest.mark.parametrize("number_of_image", [i for i in range(1, 10)])
def test_get_few_sub_breed_random_images(dog_api, number_of_image):
    response = dog_api.get(f"breed/hound/afghan/images/random/{number_of_image}")
    response = response.json()
    final_len = len(response["massage"])
    assert final_len == number_of_image, f"Кол-во фото не равно {number_of_image} а равно {final_len}"

